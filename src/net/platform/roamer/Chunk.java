package net.platform.roamer;
import java.util.*;

public class Chunk
{
	boolean[][] hasBlock;
	double seed;
	int chunkDifficulty;
	int chunkX;
	int chunkY;
	
	public Chunk(double seed, double difficultyScale, int chunkX, int chunkY)
	{
		hasBlock = new boolean[256][256];
		this.seed = seed;
		this.chunkDifficulty = (int)((difficultyScale) * Math.sqrt(chunkX * chunkX + chunkY * chunkY)) + 1;
		System.out.println(chunkDifficulty);
		this.chunkX = chunkX;
		this.chunkY = chunkY;
	}
	
	public void GeneratePlatforms(int previousPlatformX, int previousPlatformY)
	{
		Random rand = new Random();
		int lengthOfPlatforms = (int)((seed % (int)seed) * 10) + 3;//gets first decimal place in seed + 3
		int x = 0;
		int y = 0;
		
		while(x < hasBlock.length)
		{
			x = previousPlatformX + (int)(rand.nextInt(6) * chunkDifficulty);//gets the space bewtween the next platform based on difficulty of the chunk
			y = previousPlatformY + rand.nextInt(8) - 4;
			
			for(int j = 0; j < lengthOfPlatforms && x + j < hasBlock.length; j++)//iterates throught the length of the platform and places that many blocks
			{
				while(y >= hasBlock.length)//checks out of bounds
					y -= 2;
				while(y < 0)
					y += 2; 
				hasBlock[y][x + j] = true;
			}
			
			previousPlatformX = x + lengthOfPlatforms;//updates previous platform info to recently placed
			previousPlatformY = y;
		}
	}
	
	public void GeneratePlatformsBackwards(int previousPlatformX, int previousPlatformY)
	{
		Random rand = new Random();
		int lengthOfPlatforms = (int)((seed % (int)seed) * 10) + 3;//gets first decimal place in seed + 3
		int x = hasBlock.length;
		int y = 0;
		
		while(x > 0)
		{
			x = previousPlatformX + rand.nextInt(chunkDifficulty);//gets the space bewtween the next platform based on difficulty of the chunk
			y = previousPlatformY + rand.nextInt(8) - 4;
			
			for(int j = 0; j < lengthOfPlatforms && x - j > 0; j++)//iterates throught the length of the platform and places that many blocks
			{
				while(y >= hasBlock.length)//checks out of bounds
					y -= 2;
				while(y < 0)
					y += 2; 
				hasBlock[y][x - j] = true;
			}
			
			previousPlatformX = x - lengthOfPlatforms;//updates previous platform info to recently placed
			previousPlatformY = y;
		}
	}
}