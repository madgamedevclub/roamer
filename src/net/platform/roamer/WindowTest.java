package net.platform.roamer;

import java.io.IOException;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import net.platform.roamer.graphics.Player;
import net.platform.roamer.graphics.Scene;

public class WindowTest {
	public int y = 0;
	public int x = 0;
	public static final int TICK_SPEED = 30; // 30 ticks a second 1000/30
	public long nextTime;
	public int updates = 0;
	public long fps = 0;
	public long lastfps = getTime();
	public static KeyboardHandler keyHandler;
	public Texture texture;
public Scene test;
public Player player;
	public void run() {
		startup();
		nextTime = getTime() + TICK_SPEED;
		while (!Display.isCloseRequested()) {

			if (getTime() > nextTime) {
				update();
				nextTime = getTime() + TICK_SPEED;
				updates++;

			} else {
				render();
				updateFPS();
			}
		}

		Display.destroy();
	}

	public void updateFPS() {
		if (getTime() - lastfps > 1000) {
			Display.setTitle("FPS: " + fps + "Tick Speed: " + updates);
			fps = 0; // reset the FPS counter
			updates = 0;
			lastfps += 1000; // add one second
		}
		fps++;
	}

	public void startup() {
		
		try {
//			Display.setDisplayMode(Display.getDesktopDisplayMode());
			 Display.setDisplayMode(new DisplayMode(800, 600));
//			 Display.setVSyncEnabled(true);
			 Display.setResizable(true);
			// Display.setSwapInterval(2); //VSync

			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		// init OpenGL
//		GL11.glMatrixMode(GL11.GL_PROJECTION);
//		GL11.glLoadIdentity();
//		GL11.glOrtho(0, 800, 0, 600, 1, -1);
//		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		
		 GL11.glEnable(GL11.GL_TEXTURE_2D);               
         
	        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);          
	         
	            // enable alpha blending
	            GL11.glEnable(GL11.GL_BLEND);
	            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	         
	            GL11.glViewport(0,0,Display.getWidth(),Display.getHeight());
	        GL11.glMatrixMode(GL11.GL_MODELVIEW);
	 
	        GL11.glMatrixMode(GL11.GL_PROJECTION);
	        GL11.glLoadIdentity();
	        GL11.glOrtho(0, Display.getWidth(),Display.getHeight(), 0, 1, -1);
	        GL11.glMatrixMode(GL11.GL_MODELVIEW);
	        test = new Scene();
	        player = new Player(400,500,25,75);
		
		WindowTest.keyHandler = new KeyboardHandler();
		try {
			texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("assets/dude.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public void update() {
		pollInput();
		if(Keyboard.isKeyDown(Keyboard.KEY_W)){
			y--;
//			player.setMotX(player.getMotY()+2);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_S)){
			y++;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_D)){
			x++;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_A)){
			x--;
		}
		player.y = y*5;
		player.x = x*5;
		
		if (WindowTest.keyHandler.isPressed(Keyboard.KEY_F11)) {
			try {
				Display.setFullscreen(!Display.isFullscreen());
				Display.setDisplayMode(Display.getDesktopDisplayMode());
			} catch (LWJGLException e) {
				e.printStackTrace();
			}
		}
		if (WindowTest.keyHandler.isPressed(Keyboard.KEY_ESCAPE)) {
			try {
				Display.setFullscreen(false);
			} catch (LWJGLException e) {
				e.printStackTrace();
			}
		}
		player.update();

	}

	public void render() {
		// Clear the screen and depth buffer
		
//		Color.white.bind();
//		texture.bind(); // or GL11.glBind(texture.getTextureID());
//
//		GL11.glBegin(GL11.GL_QUADS);
//		GL11.glTexCoord2f(0, 0);
//		GL11.glVertex2f(100, 100);
//		GL11.glTexCoord2f(1, 0);
//		GL11.glVertex2f(100 + texture.getTextureWidth(), 100);
//		GL11.glTexCoord2f(1, 1);
//		GL11.glVertex2f(100 + texture.getTextureWidth(), 100 + texture.getTextureHeight());
//		GL11.glTexCoord2f(0, 1);
//		GL11.glVertex2f(100, 100 + texture.getTextureHeight());
//		GL11.glEnd();
//		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
//		// set the color of the quad (R,G,B,A)
//		GL11.glColor3f(0.5f, 0.5f, 1.0f);

		// draw quad
//		GL11.glBegin(GL11.GL_QUADS);
//		GL11.glVertex2f(100 + x, 100 + y);
//		GL11.glVertex2f(100 + 200 + x, 100 + y);
//		GL11.glVertex2f(100 + 200 + x, 100 + 200 + y);
//		GL11.glVertex2f(100 + x, 100 + 200 + y);
//		GL11.glEnd();
		test.render();
		player.render();
		Display.update();
	}

	public void stop() {

	}

	/**
	 * Get the time in milliseconds
	 * 
	 * @return The system time in milliseconds
	 */
	public long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	public void pollInput() {
		WindowTest.keyHandler.pollInput();

	}

	public static void main(String[] argv) {
		WindowTest quadExample = new WindowTest();
		quadExample.run();
	}
}