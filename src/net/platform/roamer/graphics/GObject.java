package net.platform.roamer.graphics;

import org.lwjgl.opengl.GL11;

public class GObject {	
		public int x;
		public int y;
		int width;
		 int height;
		 float r,g,b;
		public GObject(int x,int y, int width, int height) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			r = (float) Math.random();
			g = (float) Math.random();
			b = (float) Math.random();
		}
		public void render(){
			GL11.glColor3f(r,g,b);
			// draw quad
			GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2f(x/2 - width, y/2 + height);
			GL11.glVertex2f(x/2 - width, y/2 - height);
			GL11.glVertex2f(x/2 + width, y/2 - height);
			GL11.glVertex2f(x/2 + width, y/2 + height);
			GL11.glEnd();
		}
}
