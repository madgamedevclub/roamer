package net.platform.roamer.graphics;

import org.lwjgl.opengl.Display;

public class Scene extends GObject{

	public Scene() {
		super(Display.getWidth() << 1, Display.getHeight() << 1, Display.getWidth(), Display.getHeight());
		
	}
	@Override
	public void render() {
		this.x = Display.getWidth() << 1;
		this.y = Display.getHeight() << 1;
		this.width = Display.getWidth();
		this.height = Display.getHeight();
		super.render();
	}

}
