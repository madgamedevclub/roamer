package net.platform.roamer;

import org.lwjgl.input.Keyboard;

public class KeyboardHandler {
	public static final byte STATE_PRESSED = 1;
	public static final byte STATE_RELEASED = 2;
	public static final byte STATE_UNCHANGED = 0;
//	pu
	private byte[] keys = new byte[256];
	
	public byte getKeyState(int key){
		return keys[key];
	}
	public boolean isPressed(int key){
		return keys[key] == STATE_PRESSED;
	}
	public boolean isReleased(int key){
		return keys[key] == STATE_RELEASED;
	}
	public boolean isUnchanged(int key){
		return keys[key] == STATE_UNCHANGED;
	}
	public void pollInput(){
		keys = new byte[256];
		while (Keyboard.next()) {
			if (Keyboard.getEventKeyState()) {
				keys[Keyboard.getEventKey()] = STATE_PRESSED;
			} else {
				keys[Keyboard.getEventKey()] = STATE_RELEASED;
			}
		}
		
	}

}
